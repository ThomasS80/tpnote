﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ExerciceMonstre
{
    public class Archer : Heros
    {
        public Archer(double p_vie, double p_attaque) : base(p_vie, p_attaque)
        {
            this.m_PointDeVie = m_PointDeVie * 1.15;
            this.m_PointAttaque = m_PointAttaque * 1.15;
        }
    }
}
