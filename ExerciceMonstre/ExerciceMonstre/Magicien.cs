﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ExerciceMonstre
{
    public class Magicien : Heros
    {
        public Magicien(double p_vie, double p_attaque) : base(p_vie, p_attaque)
        {
            this.m_PointDeVie = m_PointDeVie * 1.10;
            this.m_PointAttaque = m_PointAttaque * 1.20;
        }
    }
}
