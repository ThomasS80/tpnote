﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ExerciceMonstre
{
    public class Heros : Initialisation
    {
        // génération d'un nombre aléatoire
        private static Random v_NbAleatoire = new Random();

        public Heros(double vie, double attaque) : base(vie, attaque)
        {
            this.m_PointDeVie = v_NbAleatoire.Next((int)50);
            this.m_PointAttaque = v_NbAleatoire.Next((int)20);
        }

        public void BonusElimination()
        {
            // 50 % de chance ...
            if (v_NbAleatoire.Next(0, 100) >= 50)
            {
                // ... potion de vie!
                m_PointDeVie += 15;
                Console.WriteLine("Le héros récupère des points de vie grace a une potion!");
            }
            else
            {
                // ... piège
                m_PointDeVie -= 10;
                Console.WriteLine("Oh non! Un piège fait perdre des points de vie au héro!");
                if (m_PointDeVie <= 0) m_Vivant = false;
            }
        }
        //Ces deux fonctions vont nous faire avoir les pts de vie et attaque du héro
        public double ObtenirPointDeVie()
        { 
            return this.m_PointDeVie; 
        }
        public double ObtenirPointAttaque() 
        { 
            return this.m_PointAttaque; 
        }

    }
}
