﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ExerciceMonstre
{
    //Il faut parfois lancer plusieurs fois le programme pour avoir le choix de la classe du héro comme c'est aléatoire, pour montrer que tout fonctionne
    class GestionnaireDePartie
    {
        static void Main(string[] args)
        {
            // Lancement d'une partie
            LancerPartie();
        }

        static void LancerPartie()
        {
            int v_CompteurMorts = 0;

            // Création du héro avec stats (points de vie, attaque)
            Heros v_LeHero = new Heros(100, 10);

            // Génération d'un nombre aléatoire
            Random v_NbAleatoire = new Random();

            // Création du monstre
            Monstre v_LeMonstre;

            // Tant que le héros est en vie, les combats continues
            while (v_LeHero.EstVivant())
            {
                switch (v_NbAleatoire.Next(0, 3))
                {
                    case 1:
                        v_LeMonstre = new Gobelin(20, 10);
                        break;
                    case 2:
                        v_LeMonstre = new Squelette(20, 10);
                        break;
                    case 3:
                        v_LeMonstre = new Sorciere(20, 10);
                        break;
                    default:
                        v_LeMonstre = new Gobelin(20, 10);
                        break;
                }

                while (v_LeMonstre.EstVivant() && v_LeHero.EstVivant())
                {
                    v_LeHero.Attaque(v_LeMonstre);

                    if (v_LeMonstre.EstVivant())
                    {
                        v_LeMonstre.Attaque(v_LeHero);
                    }
                }

                v_CompteurMorts++;

                // Don d'un bonus après une élimination de monstre (trésor)
                if (v_LeHero.EstVivant())
                {
                    v_LeHero.BonusElimination();
                }

                // Evolution du héros après 5 éliminations
                if (v_CompteurMorts == 5)
                {
                    bool v_SaisieValide = false;
                    string v_Saisie = "";

                    do
                    {
                        // Choix de specialisation
                        Console.WriteLine("Le héros a remporte 5 combats et peut se spécialiser");
                        Console.WriteLine("Tapez 1 pour chevalier, 2 pour archer ou 3 pour magicien");
                        v_Saisie = Console.ReadLine();

                        if (v_Saisie == "1" || v_Saisie == "2" || v_Saisie == "3")
                        {
                            v_SaisieValide = true;
                        }
                    } while (v_SaisieValide == false);
                    if (v_Saisie == "1")
                    {
                        v_LeHero = new Chevalier(60,22);
                        Console.WriteLine("Le héros devient un chevalier!");
                    }
                    else if (v_Saisie == "2")
                    {
                        v_LeHero = new Archer(57,23);
                        Console.WriteLine("Le héros devient un Archer!");
                    }
                    else if (v_Saisie == "3")
                    {
                        v_LeHero = new Magicien(55,24);
                        Console.WriteLine("Le héros devient un Magicien!");
                    }
                }
            }

            // Affichage des stats
            Console.WriteLine("\nLe héros est mort.");
            Console.WriteLine("Nombre de monstres tués : {0}", v_CompteurMorts);

        }
    }
}
