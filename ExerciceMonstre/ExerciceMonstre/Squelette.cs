﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ExerciceMonstre
{
    public class Squelette : Monstre
    {
        public Squelette(double p_vie, double p_attaque) : base(p_vie, p_attaque)
        {
            this.m_PointDeVie = m_PointDeVie * 1.2;
            this.m_PointAttaque = m_PointAttaque * 0.6;
        }
    }
}
