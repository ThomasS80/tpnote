﻿using System;

namespace ExerciceMonstre
{
    public abstract class Initialisation
    {
        protected String m_Nom;
        protected bool m_Vivant;
        protected double m_PointDeVie;
        protected double m_PointAttaque;


        public Initialisation(double p_Vie, double p_Attaque)
        {
            this.m_Nom = GetType().Name;
            this.m_PointDeVie = Math.Abs(p_Vie);
            this.m_PointAttaque = Math.Abs(p_Attaque);
            this.m_Vivant = true;

        }
        public void Attaque(Initialisation Init)
        {
            Init.EnleverPointDeVie(this.m_PointAttaque);
            Console.WriteLine(" {0} possede {1} hp et attaque un {2} ",this.m_Nom,Math.Round(m_PointDeVie,0),Init.m_Nom);
            Console.WriteLine(" PV restant : {0}  {1} ",Init.m_Nom, Math.Round(Init.m_PointDeVie, 0));

        }

        public void EnleverPointDeVie(double p_NbPointVie)
        {
            m_PointDeVie -= p_NbPointVie;
            if (m_PointDeVie <= 0)
            {
                m_Vivant = false;
            }

        }

        public bool EstVivant() { return this.m_Vivant; }
    }
}
