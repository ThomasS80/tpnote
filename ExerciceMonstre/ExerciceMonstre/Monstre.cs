﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ExerciceMonstre
{
    public class Monstre : Initialisation
    {
        public Monstre(double p_vie, double p_attaque) : base(p_vie, p_attaque)
        {
            this.m_PointDeVie = 20;
            this.m_PointAttaque = 10;
        }
    }
}
